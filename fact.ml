module type Fact =
sig
  type elt
  type mat
  type complex_mat
  type range_sampler = mat -> int -> mat
  type range_finder = mat -> mat

  (* Sample the range of a matrix. *)
  val gaussian_range_sampler : range_sampler

  (* Find an approximation of the range of a matrix. *)
  val randomized_range_finder :
    ?sampler:range_sampler -> int -> range_finder
  val adaptive_randomized_range_finder :
    ?sampler:range_sampler -> ?r:int -> float -> range_finder
  val randomized_subspace_iteration :
    ?sampler:range_sampler -> int -> int -> range_finder
  val adaptive_randomized_subspace_iteration :
    ?sampler:range_sampler -> ?r:int -> int -> float -> range_finder

  (* Factorize a matrix using its range. *)
  val direct_svd : mat -> mat -> mat * mat * mat
  val direct_eig : mat -> mat -> complex_mat * complex_mat
  val nystrom_eig : mat -> mat -> mat * mat

  (* Combine a range finder and a postprocessor. *)
  val approximate : range_finder -> (mat -> mat -> 'a) -> mat -> 'a                                  
end



module Make (M : Matrix.Matrix) : Fact with type elt = M.elt
                                        and type mat = M.mat
                                        and type complex_mat = M.complex_mat =
struct
  type elt = M.elt
  type mat = M.mat
  type complex_mat = M.complex_mat
  type range_sampler = mat -> int -> mat
  type range_finder = mat -> mat

  let _check_argument (condition : bool) (error_msg : string) =
    if not (condition) then
      raise (Invalid_argument error_msg)

  
  (* sample k vectors in the image of A *)
  let gaussian_range_sampler (a : mat) (k : int) =
    let _, n = M.shape a in 
    let samples = M.gaussian n k in
    M.dot a samples


  (* Algorithm 4.4 *)
  (** If [sampler] is not specified, it defaults to {!val:gaussian_range_sampler}. *)
  let randomized_subspace_iteration
        ?sampler:(sampler=gaussian_range_sampler)
        (power : int) (size : int) (a : mat) : mat =
    _check_argument (size >= 0) "family size must be greater than 0" ;
    _check_argument (power >= 0) "power must be greater than 0" ;
    
    let samples = sampler a size in
    let q, _ = M.qr samples in
    let a_star = M.ctranspose a in
    (* main loop *)
    let rec loop q = function
      | 0 -> q
      | n -> (let y_tilde = M.dot a_star q in
              let q_tilde, _ = M.qr y_tilde  in
              let y = M.dot a q_tilde in
              let q, _ = M.qr y in
              loop q (n - 1)) in
    loop q power

  
  (* Algorithm 4.1 *)
  (** If [sampler] is not specified, it defaults to {!val:gaussian_range_sampler}. *)
  let randomized_range_finder 
        ?sampler:(sampler=gaussian_range_sampler) : int -> mat -> mat =
    randomized_subspace_iteration ~sampler 0 


  let _fast_pow (a : mat) : int -> mat =
    let m, n = M.shape a in
    _check_argument (m = n) "the matrix must be square" ;
    let rec aux accumulator base = function
      | 0 -> accumulator
      | 1 -> M.dot base accumulator
      | e when e mod 0 = 0 -> aux accumulator (M.dot base base) (e / 2)
      | e -> aux (M.dot base accumulator) (M.dot base base) ((e - 1) / 2) in
    aux (M.eye m) a
      

  (* Adaptive version of Algorithm 4.4 *)
  (** If [sampler] is not specified, it defaults to {!val:gaussian_range_sampler}. *)
  let adaptive_randomized_subspace_iteration
        ?sampler:(sampler=gaussian_range_sampler) ?r:(r=10)
        (power : int) (precision : float ) (a : mat) : mat =
    _check_argument (r >= 2) "r must be greater than 2" ;
    _check_argument (power >= 0) "power must be greater than 0" ;
    _check_argument (precision > 0.) "precision must be strictly greater than 0" ;
    
    (* constants *)
    let m, _ = M.shape a in
    let first_col_slice = [ [] ; [0] ] in
    let last_cols_slice = [ [] ; [1 ; r-1] ] in
    let nbr_iter = 2 * power in
    let bound = (precision ** (float (nbr_iter + 1))) /.
                (10. *. Float.sqrt (2. *. Float.pi)) in
    let a_iter = M.dot (_fast_pow (M.dot a @@ M.ctranspose a) power) a in
    let sampler = sampler a_iter in

    (* initialization *)
    (* w ~ N(0,1)^(n x r) *)
    (* y = (AA^H)^qAw *)
    (* Q_0.shape = (m,0) *)
    let samples = sampler r in
    let y = ref samples in
    let q = ref (M.zeros m 0) in
    
    (* functions for intermediate computations *)
    let gram_schmidt_step (q : mat) (vec : mat) : mat =
      M.sub
        vec
        (M.dot
           (M.dot
              q
              (M.ctranspose q))
           vec) in

    (* main loop *)
    while M.to_float (M.max' (M.l2norm ~axis:0 !y)) >= bound do
      let old_y_1 = M.get_slice first_col_slice !y in
      let old_y_2r = M.get_slice last_cols_slice !y in

      (* compute the new range approximation *)
      (* y_j = ( I - Q_{j-1}Q_{j-1}* )y_j *)
      let projected_y_1 = gram_schmidt_step !q old_y_1 in
      (* q_j = y_j / ||y_j|| *)
      let new_q_col = M.div_scalar
                        projected_y_1
                        (M.l2norm' projected_y_1) in
      (* Q_j = [ Q_{j-1} q_j ] *)
      q := M.concat_horizontal !q new_q_col ;

      (* w ~ N(0,1)^n *)
      (* sample = (AA^H)^qAw *)
      (* y_{j+r} = ( I - Q_jQ_j* )sample *)
      let sample = sampler 1 in
      let new_y_col = gram_schmidt_step !q sample in
      (* y_i = y_i - (q_j,y_i)q_j *)
      (* y = [ y_{j+1} ... y_{j+r} ] *)
      let new_y_2r = gram_schmidt_step new_q_col old_y_2r in
      y := M.concat_horizontal new_y_2r new_y_col
    done ;
    !q

  
  (* Algorithm 4.2 *)
  (** If [sampler] is not specified, it defaults to {!val:gaussian_range_sampler}. *)
  let adaptive_randomized_range_finder 
    ?sampler:(sampler=gaussian_range_sampler) ?r:(r=10) : float -> mat -> mat =
    adaptive_randomized_subspace_iteration ~sampler ~r 0


  let _check_range_shape (a : mat) (range : mat) : unit =
    _check_argument
      (fst (M.shape a) = fst (M.shape range))
      "A and its range must have the same number of rows"
  
  (* Algorithm 5.1 *)
  let direct_svd (a : mat) (range : mat) : mat * mat * mat =
    _check_range_shape a range ;
    let b = M.dot (M.ctranspose range) a in
    let u_tilde, sigma, v = M.svd b in
    let u = M.dot range u_tilde in
    u, sigma, v


  (* Algorithm 5.3 *)
  let direct_eig (a : mat) (range : mat) : complex_mat * complex_mat =
    _check_range_shape a range ;
    let b = M.dot
              (M.dot
                 (M.ctranspose range)
                 a)
              range in
    let v, lambda = M.eig b in
    let u = M.dot_complex (M.cast range) v in
    u, lambda


  (* Algorithm 5.5 *)
  let nystrom_eig (a : mat) (range : mat) : mat * mat =
    _check_range_shape a range ;
    let b1 = M.dot a range in
    let b2 = M.dot (M.ctranspose range) b1 in
    let c = M.chol b2 in
    let f = M.triangular_solve c b1 in
    let u, sigma, _ = M.svd f in
    let lambda = M.mul sigma sigma in
    u, lambda


  let approximate (range_finder : range_finder) (postprocessor : mat -> mat -> 'a) (a : mat) : 'a =
    a |> range_finder |> postprocessor a
end


module S : Fact with type elt = Owl.Dense.Matrix.S.elt
                 and type mat = Owl.Dense.Matrix.S.mat
                 and type complex_mat = Owl.Dense.Matrix.C.mat =
  Make (Matrix.S)
                                  
module D : Fact with type elt = Owl.Dense.Matrix.D.elt
                 and type mat = Owl.Dense.Matrix.D.mat
                 and type complex_mat = Owl.Dense.Matrix.Z.mat =
  Make (Matrix.D)

module C : Fact with type elt = Owl.Dense.Matrix.C.elt
                 and type mat = Owl.Dense.Matrix.C.mat
                 and type complex_mat = Owl.Dense.Matrix.C.mat =
  Make (Matrix.C)

module Z : Fact with type elt = Owl.Dense.Matrix.Z.elt
                 and type mat = Owl.Dense.Matrix.Z.mat
                 and type complex_mat = Owl.Dense.Matrix.Z.mat =
  Make (Matrix.Z)
