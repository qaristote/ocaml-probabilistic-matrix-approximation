module type Matrix =
sig
  type elt
  type mat
  type complex_mat
    
  (* Convert between real and complex values. *)
  val to_float : elt -> float
  val cast : mat -> complex_mat

  (* Get basic properties of a matrix. *)
  val shape : mat -> int * int

  (* Generate a matrix. *)
  val zeros : int -> int -> mat
  val eye : int -> mat
  val gaussian : ?mu:elt -> ?sigma:elt -> int -> int -> mat

  (* Algebra on matrices. *)   
  val sub : mat -> mat -> mat
  val dot : mat -> mat -> mat
  val dot_complex : complex_mat -> complex_mat -> complex_mat
  val mul : mat -> mat -> mat
  val div_scalar : mat -> elt -> mat
  val triangular_solve : mat -> mat -> mat

  (* Get advanced properties of a matrix. *)
  val max' : mat -> elt
  val l2norm : ?axis:int -> ?keep_dims:bool -> mat -> mat
  val l2norm' :  mat -> elt

  (* Operations on the rows and columns of matrices. *)
  val get_slice : int list list -> mat -> mat
  val transpose : mat -> mat
  val ctranspose : mat -> mat
  val concat_horizontal : mat -> mat -> mat

  (* Standard matrix factorizations. *)
  val qr : mat -> mat * mat
  val svd : mat -> mat * mat * mat
  val eig : mat -> complex_mat * complex_mat
  val chol : mat -> mat
end


module MakeFromOwlCommon
    (M : Owl_dense_matrix_intf.Common)
    (L : Owl_linalg_intf.Common with type elt = M.elt
                                 and type mat = M.mat) =
struct
  include M
  include L

  let triangular_solve (a : mat) (b : mat) =
    let x = L.triangular_solve ~upper:true ~trans:true a (M.ctranspose b) in
    M.ctranspose x

  let qr (x : mat) =
    let q, r, _ = L.qr x in
    q,r

  let svd (x : mat) = L.svd x

  let eig (x : mat) = L.eig x

  let chol (x : mat) = L.chol x
end

module MakeFromOwlReal
    (M : Owl_dense_matrix_intf.Common with type elt = float)
    (L : Owl_linalg_intf.Common with type elt = M.elt
                                 and type mat = M.mat)
    (CM : Owl_dense_matrix_intf.Common with type elt = Complex.t
                                        and type mat = L.complex_mat) =
struct
  include MakeFromOwlCommon (M) (L)

  let to_float (x : elt) : float = x
    
  let dot_complex = CM.dot
end


module MakeFromOwlComplex
    (M : Owl_dense_matrix_intf.Common with type elt = Complex.t)
    (L : Owl_linalg_intf.Common with type elt = M.elt
                                 and type mat = M.mat
                                 and type complex_mat = M.mat) :
  Matrix with type elt = Complex.t
          and type mat = M.mat
          and type complex_mat = M.mat =
struct
  include MakeFromOwlCommon (M) (L)

  let to_float : elt -> float = function
    | { re = x ; im = 0. } -> x
    | _ -> raise (Invalid_argument "cannot convert non-real value to float")

  let dot_complex : complex_mat -> complex_mat -> complex_mat = M.dot

  let cast (x : mat) : complex_mat = x
end


module S : Matrix with type elt = Owl.Dense.Matrix.S.elt
                   and type mat = Owl.Dense.Matrix.S.mat
                   and type complex_mat = Owl.Dense.Matrix.C.mat =
struct
  include MakeFromOwlReal (Owl.Dense.Matrix.S) (Owl.Linalg.S) (Owl.Dense.Matrix.C)

  let cast = Owl.Dense.Matrix.Generic.cast_s2c
end
    
module D : Matrix with type elt = Owl.Dense.Matrix.D.elt
                   and type mat = Owl.Dense.Matrix.D.mat
                   and type complex_mat = Owl.Dense.Matrix.Z.mat =
struct
  include MakeFromOwlReal (Owl.Dense.Matrix.D) (Owl.Linalg.D) (Owl.Dense.Matrix.Z)

  let cast = Owl.Dense.Matrix.Generic.cast_d2z
end
    
module C : Matrix with type elt = Owl.Dense.Matrix.C.elt
                   and type mat = Owl.Dense.Matrix.C.mat                                    
                   and type complex_mat = Owl.Dense.Matrix.C.mat =
  MakeFromOwlComplex (Owl.Dense.Matrix.C) (Owl.Linalg.C)
    
module Z : Matrix with type elt = Owl.Dense.Matrix.Z.elt
                   and type mat = Owl.Dense.Matrix.Z.mat
                   and type complex_mat = Owl.Dense.Matrix.Z.mat =
  MakeFromOwlComplex (Owl.Dense.Matrix.Z) (Owl.Linalg.Z)

