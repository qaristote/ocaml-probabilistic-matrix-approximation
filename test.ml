module type MatrixToTest =
sig
  include Matrix.Matrix
end


module type Test =
sig
  type elt
  type mat
  type range_finder
  
  (* Compute an error. *)

  val error_factorization : mat list -> mat -> elt
  val error_range_finder : range_finder -> mat -> elt

  (*  Test the precision of an algorithm. *)
  
  val test_error : elt -> float -> bool
  val test_adaptive_range_finder : (float -> range_finder) -> float -> mat -> bool
end


module Make (M : MatrixToTest) (F : Fact.Fact with type elt = M.elt
                                               and type mat = M.mat) =
struct      
  type elt = M.elt
  type mat = M.mat      
  type range_finder = F.range_finder

  (* Compute an error *)
                        
  let error_factorization (factors : mat list) (a : mat) : elt =
    let m, _ = M.shape a in
    (List.fold_left (fun (a : mat) (b : mat) ->
                       if (snd (M.shape a) <> 1) && (fst (M.shape b) = 1) then
                         M.mul a b
                       else
                         M.dot a b)
        (M.eye m) factors)
    |> M.sub a
    |> M.l2norm'

  let error_range_finder (range_finder : range_finder) (a : mat) : elt =
    let q = range_finder a in
    error_factorization [ q ; M.ctranspose q ; a ] a  

  (* Test the precision of an algorithm *)
      
  let test_error (error : elt) (target : float) : bool =
    M.to_float error <= target

  let test_adaptive_range_finder (adaptive_range_finder : float -> range_finder) (epsilon : float) (a : mat) : bool =
    let error = error_range_finder (adaptive_range_finder epsilon) a in
    test_error error epsilon
end


module S : Test with type elt = Owl.Dense.Matrix.S.elt
                 and type mat = Owl.Dense.Matrix.S.mat
                 and type range_finder = Fact.S.range_finder =
  Make (Matrix.S) (Fact.S)

module D : Test with type elt = Owl.Dense.Matrix.D.elt
                 and type mat = Owl.Dense.Matrix.D.mat
                 and type range_finder = Fact.D.range_finder =
  Make (Matrix.D) (Fact.D)

module C : Test with type elt = Owl.Dense.Matrix.C.elt
                 and type mat = Owl.Dense.Matrix.C.mat
                 and type range_finder = Fact.C.range_finder =
  Make (Matrix.C) (Fact.C)

module Z : Test with type elt = Owl.Dense.Matrix.Z.elt
                 and type mat = Owl.Dense.Matrix.Z.mat
                 and type range_finder = Fact.Z.range_finder =
  Make (Matrix.Z) (Fact.Z)
