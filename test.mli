module type MatrixToTest =
sig
  include Matrix.Matrix
end



module type Test =
sig
  type elt
  type mat
  type range_finder

  
  (** {2 Computing an error.} *)

  
  val error_factorization : mat list -> mat -> elt
    
  val error_range_finder : range_finder -> mat -> elt

  
  (** {2 Testing the precision of an algorithm.} *)

  
  val test_error : elt -> float  -> bool

  val test_adaptive_range_finder : (float -> range_finder) -> float -> mat -> bool
end



module Make (M : MatrixToTest) (F : Fact.Fact with type elt = M.elt
                                               and type mat = M.mat) : Test



(** Library testing for Owl's [float32] matrices. *)
module S : Test with type elt = Owl.Dense.Matrix.S.elt
                 and type mat = Owl.Dense.Matrix.S.mat
                 and type range_finder = Fact.S.range_finder

(** Library testing for Owl's [float64] matrices. *)
module D : Test with type elt = Owl.Dense.Matrix.D.elt
                 and type mat = Owl.Dense.Matrix.D.mat
                 and type range_finder = Fact.D.range_finder

(** Library testing for Owl's [complex32] matrices. *)
module C : Test with type elt = Owl.Dense.Matrix.C.elt
                 and type mat = Owl.Dense.Matrix.C.mat
                 and type range_finder = Fact.C.range_finder

(** Library testing for Owl's [complex64] matrices. *)
module Z : Test with type elt = Owl.Dense.Matrix.Z.elt
                 and type mat = Owl.Dense.Matrix.Z.mat
                 and type range_finder = Fact.Z.range_finder                                           
