(** The signature of the algorithms implemented in this library. *)
module type Fact =
sig
  type elt
  type mat
  type complex_mat

  
  (** {2 Sampling the range of a matrix.} *)

  
  type range_sampler = mat -> int -> mat

  
  (** [gaussian_range_sampler a n] returns [n] random independant gaussian 
      vectors in the range of [a]. *) 
  val gaussian_range_sampler : range_sampler
    
  
  (** {2 Finding an approximation of the range of a matrix.} *)

  
  type range_finder = mat -> mat

  
  (** [randomized_range_finder ~sampler l a] returns an orthonormal family of 
      size [l] that approximates the range of [a].  *)
  val randomized_range_finder :
    ?sampler:range_sampler -> int -> range_finder
  (** It is obtained by sampling a family of [l] vectors in the range of [a] 
      using [sampler] and computing its QR factorization. In particular, if [l] 
      is greater than the rank of [a], the outputed family will be truncated.

      If [k] is the desired rank of the family, a good value for [l] is around 
      [k+5~k+10].

      @see <https://doi.org/10.1137/090771806> 
      {i Finding Structure with Randomness: Probabilistic Algorithms for 
      Constructing Approximate Matrix Decompositions}, Algorithm 4.1 *) 

  
  (** [adaptive_randomized_range_finder ~sampler ~r eps a] returns an 
      orthonormal family of that approximates the range of [a] with precision 
      [eps]. *)
  val adaptive_randomized_range_finder :
    ?sampler:range_sampler -> ?r:int -> float -> range_finder
  (** If [a] is of size [m x n], [q] is the resulting orthonormal matrix and 
      [q^H] its adjoint, then [(id - qq^H)a] has norm smaller than [eps] with 
      probability at least [1 - min(m,n)1e-r].

      It is obtained by sampling a vectors in the range of [a] using [sampler] 
      and computing the QR factorisation of the resulting family until 
      sufficient precision is reached.

      @see <https://doi.org/10.1137/090771806> 
      {i Finding Structure with Randomness: Probabilistic Algorithms for 
      Constructing Approximate Matrix Decompositions}, Algorithm 4.2 *) 

  
  (** [randomized_subspace_iteration ~sampler l q a] returns an orthonormal 
      family of size [l] that approximates the range of [a]. *)
  val randomized_subspace_iteration :
    ?sampler:range_sampler -> int -> int -> range_finder
  (** It is obtained by sampling a family of [l] vectors in the range of 
      [(aa^H)^qa] (where [a^H] is the adjoint of [a]) using [sampler] and 
      computing its QR factorization.  In particular, if [l] is greater than
      the rank of [a], the outputed family will be truncated. This method 
      improves the accuracy of {!val:randomized_range_finder} for those matrices
      whose singular values decay slowly.

      If [k] is the desired rank of the family, a good value for [l] is around 
      [k+5~k+10].
      @see <https://doi.org/10.1137/090771806> 
      {i Finding Structure with Randomness: Probabilistic Algorithms for 
      Constructing Approximate Matrix Decompositions}, Algorithm 4.4 *)

  (** [adaptive_randomized_subspace_iteration ~sampler ~r q eps a] returns an
      orthonormal family of that approximates the range of [a] with precision 
      [eps]. *)
  val adaptive_randomized_subspace_iteration :
    ?sampler:range_sampler -> ?r:int -> int -> float -> range_finder
  (** If [a] is of size [m x n], [q] is the resulting orthonormal matrix and 
      [q^H] its adjoint, then [(id - qq^H)a] has norm smaller than [eps] with 
      probability at least [1 - min(m,n)1e-r].

      It is obtained by sampling vectors in the range of [(aa^H)^qa] using 
      [sampler] (where [a^H] is the adjoint of [a]) and computing the QR 
      factorisation of the resulting family until sufficient precision is 
      reached. This method improves the rate of accuracy of 
      {!val:adaptive_randomized_range_finder} for those matrices whose singular 
      values decay slowly.

      @see <https://doi.org/10.1137/090771806> 
      {i Finding Structure with Randomness: Probabilistic Algorithms for 
      Constructing Approximate Matrix Decompositions}, Section 4.5 *) 

  
  (** {2 Factorizing a matrix using (an approximation of) its range.} *)

  
  (** If [q] approximates the range of [a] with precision [eps], 
      [direct_eig a q] computes an approximate singular value decomposition 
      [(u,sigma,v)] of [a] with precision [eps]. *)
  val direct_svd : mat -> mat -> mat * mat * mat
  (** @see <https://doi.org/10.1137/090771806> 
      {i Finding Structure with Randomness: Probabilistic Algorithms for 
      Constructing Approximate Matrix Decompositions}, Algorithm 5.1 *)

  
  (** If [q] approximates the range of a hermitian matrix [a] with precision 
      [eps], [direct_eig a q] computes an approximate decomposition [(u,lambda)] 
      of [a] into eigenvectors [u] and eigenvalues [lambda] with precision 
      [2eps]. *)
  val direct_eig : mat -> mat -> complex_mat * complex_mat
  (** @see <https://doi.org/10.1137/090771806> 
      {i Finding Structure with Randomness: Probabilistic Algorithms for 
      Constructing Approximate Matrix Decompositions}, Algorithm 5.3 *)

  (** If [q] approximates the range of a positive semi-definite matrix [a] with 
      precision [eps], [direct_eig a q] computes an approximate decomposition 
      [(u,lambda)] of [a] into eigenvectors [u] and eigenvalues [lambda] with 
      precision [2eps]. *)
  val nystrom_eig : mat -> mat -> mat * mat
  (** This method improves has roughly the same running time as 
      {!val:direct_eig} but is typically much more accurate.
      
      @see <https://doi.org/10.1137/090771806> 
      {i Finding Structure with Randomness: Probabilistic Algorithms for 
      Constructing Approximate Matrix Decompositions}, Algorithm 5.5 *)

  
  (** {2 Combining a range finder and a postprocessor.} *)

  
  (** [postprocessor range_finder postprocessor a] returns a probabilistic
      approximation of [a] using [range_finder] to find its range and
      [postprocessor] to derive its approximation. *)
  val approximate : range_finder -> (mat -> mat -> 'a) -> mat -> 'a
end



(** Generate probabilistic matrix approximation functions from a linear algebra 
      library. *)
module Make : Matrix.Matrix -> Fact




(** Probabilistic matrix approximation functions for Owl's [float32] 
    matrices. *)
module S : Fact with type elt = Owl.Dense.Matrix.S.elt
                 and type mat = Owl.Dense.Matrix.S.mat
                 and type complex_mat = Owl.Dense.Matrix.C.mat
  
(** Probabilistic matrix approximation functions for Owl's [float64] 
    matrices. *)
module D : Fact with type elt = Owl.Dense.Matrix.D.elt
                 and type mat = Owl.Dense.Matrix.D.mat
                 and type complex_mat = Owl.Dense.Matrix.Z.mat
                                          
(** Probabilistic matrix approximation functions for Owl's [complex32] 
    matrices. *)
module C : Fact with type elt = Owl.Dense.Matrix.C.elt
                 and type mat = Owl.Dense.Matrix.C.mat
                 and type complex_mat = Owl.Dense.Matrix.C.mat
                                          
(** Probabilistic matrix approximation functions for Owl's [complex64] 
    matrices. *)
module Z : Fact with type elt = Owl.Dense.Matrix.Z.elt
                 and type mat = Owl.Dense.Matrix.Z.mat
                 and type complex_mat = Owl.Dense.Matrix.Z.mat
