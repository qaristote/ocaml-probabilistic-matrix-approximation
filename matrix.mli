(** The interface for linking with a linear algebra library. *)
module type Matrix =
sig
  (** Unless specified otherwise, the types and values are the same as in the 
      Owl library. *)
  
  type elt
  type mat
  type complex_mat

  (** {2 Conversions between real and complex values.} *)

  
  (** Convert a real number of type [elt] to [float]. *)
  val to_float : elt -> float

  (** Convert a matrix whose elements are of type [elt] to a complex matrix. *)
  val cast : mat -> complex_mat

  
  (** {2 Basic properties of matrices.} *)
    
  val shape : mat -> int * int

  
  (** {2 Matrix creation.} *)
                     
  val zeros : int -> int -> mat
  val eye : int -> mat
  val gaussian : ?mu:elt -> ?sigma:elt -> int -> int -> mat

  
  (** {2 Algebra with matrices.} *)
    
  val sub : mat -> mat -> mat
  val dot : mat -> mat -> mat
  val dot_complex : complex_mat -> complex_mat -> complex_mat
  val mul : mat -> mat -> mat
  val div_scalar : mat -> elt -> mat
 
  (** [triangular_solve a b] returns the solution [x] of the linear system 
      [x * a = b]. *)
  val triangular_solve : mat -> mat -> mat

  
  (** {2 Advanced properties of matrices.} *)
    
  val max' : mat -> elt
  val l2norm : ?axis:int -> ?keep_dims:bool -> mat -> mat
  val l2norm' :  mat -> elt

  
  (** {2 Operations on the rows and columns of matrices.} *)
    
  val get_slice : int list list -> mat -> mat
  val transpose : mat -> mat
  val ctranspose : mat -> mat
  val concat_horizontal : mat -> mat -> mat

  
  (** {2 Standard matrix factorizations.} *)
    
  val qr : mat -> mat * mat
  val svd : mat -> mat * mat * mat
  val eig : mat -> complex_mat * complex_mat
  val chol : mat -> mat
end



(** Linear algebra for Owl's [float32] matrices. *)
module S : Matrix with type elt = Owl.Dense.Matrix.S.elt
                   and type mat = Owl.Dense.Matrix.S.mat
                   and type complex_mat = Owl.Dense.Matrix.C.mat

(** Linear algebra for Owl's [float64] matrices. *)
module D : Matrix with type elt = Owl.Dense.Matrix.D.elt
                   and type mat = Owl.Dense.Matrix.D.mat
                   and type complex_mat = Owl.Dense.Matrix.Z.mat
                                            
(** Linear algebra for Owl's [complex32] matrices. *)
module C : Matrix with type elt = Owl.Dense.Matrix.C.elt
                   and type mat = Owl.Dense.Matrix.C.mat
                   and type complex_mat = Owl.Dense.Matrix.C.mat
                                            
(** Linear algebra for Owl's [complex64] matrices. *)
module Z : Matrix with type elt = Owl.Dense.Matrix.Z.elt
                   and type mat = Owl.Dense.Matrix.Z.mat
                   and type complex_mat = Owl.Dense.Matrix.Z.mat
