# Introduction 

The Probabilistic Matrix Approximation (PMA) library is an implementation in OCaml of the algorithms described in the article [Finding Structure with Randomness: Probabilistic Algorithms for Constructing Approximate Matrix Decompositions](https://doi.org/10.1137/090771806).

Currently, not all variations of the algorithms are implemented, but the main versions are. The library focuses on modularity : although it is the most easily used along [Owl](https://ocaml.xyz), it can in practice be used with any linear algebra library, by linking it in a module. Read more in the documentation and in [the demo notebook](./demo.ipynb).

# Building

To build the library, use 
``` 
dune build
```
The binary files will be available in the `_build` directory.

To build the documentation, use
```
dune build @doc-private
```
The documentation can then be accessed by opening the `html` files in `_build/default/_doc/_html`.
